/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.retail.posterminal.OBPOSApplications;

public class SharafReportsUtil {

  public static String getFilterClauseForStatus(String status) {
    String whereClause = "";
    final String notReturnClause = " and ct.isreturn = 'N'";
    final String docSubTypeOBClause = " and ct.docsubtypeso like 'OB'";
    final String docSubTypeNotOBClause = " and ct.docsubtypeso not like 'OB'";
    final String isDeletedClause = " and co.em_obpos_isdeleted = 'N' ";

    switch (status) {
    case "ALL":
      whereClause += docSubTypeNotOBClause;
      whereClause += isDeletedClause;
      break;
    case "UE":
    	whereClause += isDeletedClause;
    case "CJ":
      whereClause += " and  co.docstatus = '" + status + "'";
      whereClause += docSubTypeOBClause;
      whereClause += notReturnClause;
      whereClause += isDeletedClause;
      break;
    case "SQ":
      whereClause += " and exists (select 1 from c_orderline qol ";
      whereClause += "             where qol.c_order_id = co.c_order_id";
      whereClause += "               and qol.em_shaquo_order_id is not null) ";
      whereClause += " and co.docstatus IN ('CO', 'CL')";
      whereClause += docSubTypeNotOBClause;
      whereClause += notReturnClause;
      whereClause += isDeletedClause;
      break;
    case "SO":
      whereClause += " and not exists (select 1 from c_orderline qol ";
      whereClause += "             where qol.c_order_id = co.c_order_id";
      whereClause += "               and qol.em_shaquo_order_id is not null) ";
      whereClause += " and co.docstatus IN ('CO', 'CL')";
      whereClause += docSubTypeNotOBClause;
      whereClause += notReturnClause;
      whereClause += isDeletedClause;
      break;
    case "ALLV":
      whereClause += docSubTypeNotOBClause;
      break;
    default:
    }

    return whereClause;
  }

  public static List<BigDecimal> getTotalDebtorSalesByCashierAndTill(String cashierId,
      String tillId, Date dateFrom, Date dateTo) {
    List<BigDecimal> amounts = new ArrayList<BigDecimal>();

    if (StringUtils.isEmpty(cashierId)) {
      // No cashier given, return zeroes
      amounts.add(BigDecimal.ZERO);
      amounts.add(BigDecimal.ZERO);
      amounts.add(BigDecimal.ZERO);
      return amounts;
    }

    OBCriteria<FIN_PaymentSchedule> debtorOrders = OBDal.getInstance().createCriteria(
        FIN_PaymentSchedule.class);
    debtorOrders.setFilterOnReadableOrganization(false);
    debtorOrders.createAlias(FIN_PaymentSchedule.PROPERTY_ORDER, "o");
    debtorOrders.createAlias("o." + Order.PROPERTY_DOCUMENTTYPE, "dt");
    debtorOrders.add(Restrictions.eq("o." + Order.PROPERTY_SALESTRANSACTION, true));
    debtorOrders.add(Restrictions.eq("o." + Order.PROPERTY_PROCESSED, true));
    debtorOrders.add(Restrictions.eq("o." + Order.PROPERTY_CREATEDBY,
        OBDal.getInstance().getProxy(User.class, cashierId)));
    debtorOrders.add(Restrictions.eq("o." + Order.PROPERTY_OBPOSAPPLICATIONS, OBDal.getInstance()
        .getProxy(OBPOSApplications.class, tillId)));
    debtorOrders.add(Restrictions.ge("o." + Order.PROPERTY_POSSBUSINESSDATE, dateFrom));
    debtorOrders.add(Restrictions.le("o." + Order.PROPERTY_POSSBUSINESSDATE, dateTo));

    ProjectionList projections = Projections.projectionList();
    projections.add(Projections.groupProperty("dt." + DocumentType.PROPERTY_RETURN));
    projections.add(Projections.sum(FIN_PaymentSchedule.PROPERTY_OUTSTANDINGAMOUNT));
    debtorOrders.setProjection(projections);

    BigDecimal totalDebtorSales = BigDecimal.ZERO;
    BigDecimal totalDebtorRefunds = BigDecimal.ZERO;

    for (Object record : debtorOrders.list()) {
      Object[] debtorAmount = (Object[]) record;
      boolean isReturn = (boolean) debtorAmount[0];
      BigDecimal amount = (BigDecimal) debtorAmount[1];
      if (isReturn) {
        totalDebtorRefunds = amount.negate();
      } else {
        totalDebtorSales = amount;
      }
    }
    amounts.add(totalDebtorSales);
    amounts.add(totalDebtorRefunds);
    amounts.add(totalDebtorSales.subtract(totalDebtorRefunds));

    return amounts;
  }
}

/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2017 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.sharaf.reports;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.ReportDefinition;
import org.openbravo.client.application.report.BaseReportActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.client.kernel.reference.UIDefinitionController;
import org.openbravo.client.kernel.reference.UIDefinitionController.FormatDefinition;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.model.ad.system.Client;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * Action Handler used as base for jasper reports with subreports generated from process definition.
 * This handler can be extended to customize its behavior.
 * 
 */
public class BaseSubReportActionHandler extends BaseReportActionHandler {
  public static final Logger log = Logger.getLogger(BaseSubReportActionHandler.class);

  String refundListingReportId = "21A1FE5929DA4C538B289917CA05E711";
  String netSalesSummaryReportId = "52FED0D1E5AF4738A8CFB774982A223A";

  @Override
  protected void addAdditionalParameters(ReportDefinition report, JSONObject jsonContent,
      Map<String, Object> parameters) {
    try {
      String processDefinid = report.getProcessDefintion().getId();
      ConnectionProvider conn;
      if (processDefinid.equalsIgnoreCase(refundListingReportId)
          || processDefinid.equalsIgnoreCase(netSalesSummaryReportId)) {
        conn = DalConnectionProvider.getReadOnlyConnectionProvider();
      } else {
        conn = new DalConnectionProvider(false);
      }

      parameters.put("REPORT_CONNECTION", conn.getConnection());

      VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();
      final FormatDefinition reportFormat = UIDefinitionController.getInstance()
          .getFormatDefinition("amount", UIDefinitionController.NORMALFORMAT_QUALIFIER);

      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
      dfs.setDecimalSeparator(reportFormat.getDecimalSymbol().charAt(0));
      dfs.setGroupingSeparator(reportFormat.getGroupingSymbol().charAt(0));

      final String strTmpDecSymbol = "xxTmpDecSymbolxx";
      String localMask = reportFormat.getFormat().replace(reportFormat.getDecimalSymbol(),
          strTmpDecSymbol);
      localMask = localMask.replace(reportFormat.getGroupingSymbol(), ",");

      String clientID = OBContext.getOBContext().getCurrentClient().getId();
      Client client = OBDal.getInstance().get(Client.class, clientID);

      final DecimalFormat numberFormat = new DecimalFormat(localMask.replace(strTmpDecSymbol, "."),
          dfs);
      numberFormat.setMinimumFractionDigits(client.getCurrency().getStandardPrecision().intValue());
      numberFormat.setMaximumFractionDigits(client.getCurrency().getStandardPrecision().intValue());
      parameters.put("STDPRECISIONAMOUNTFORMAT", numberFormat);
    } catch (Exception e) {
      log.info("Exception During Custom reports: " + report.getProcessDefintion().getName()
          + " export." + e.getMessage());
      e.printStackTrace();
    }
  }

  @Override
  protected boolean isCompilingSubreports() {
    return true;
  }

}
